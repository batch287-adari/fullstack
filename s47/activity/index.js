const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name')
const spanFullName = document.querySelector('#span-full-name');
function updateSpanContent(){
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
};
txtFirstName.addEventListener('keyup', (event) => {
	updateSpanContent();
});
txtLastName.addEventListener('keyup', (event) => {
	updateSpanContent();
});

