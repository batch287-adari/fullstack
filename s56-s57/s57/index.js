// Note: You can use the snippets here to demonstrate it in the browser console.

// Question: What would be the output of the loop?

for (let i = 1; i < 5; i++) {
  console.log(i * 1);
}
// Answer
/*
  1
  2
  3
  4

*/
// Question: What would be the problem in the code snippet?

let students = ['John', 'Paul', 'George', 'Ringo'];

console.log('Here are the graduating students:');

for (let count = 0; count <= students.length; i++) {
  console.log(students[count]);
}

// Answer
/*
  It should be count<students.length otherwise the index will be out of bounds.
*/

// Question: What would be the console output of the function?

function checkGift(day) {
  let gifts = [
    'partridge in a pear tree',
    'turtle doves',
    'french hens',
    'golden rings'
  ];

  if (day > 0 && day < 4) {
    return `I was given ${day} ${gifts[day-1]}`;
  } else {
    return `No gifts were given`;
  }
}

checkGift(3);
// Answer
/*
  No output since instance of day variable is not created inside the function but we used in if condition.
*/

// What would be the problem in the code snippet?

let items = [
  {
    id: 1,
    name: 'Banana',
    description: 'A yellow fruit',
    price: 15.00,
    category: 2
  },
  {
    id: 2,
    name: 'Pork Cutlet',
    description: 'Japanese kurobuta',
    price: 15.00,
    category: 1
  }, 
  {
    id: 1,
    name: 'Sweet Potato',
    description: 'Best when roasted',
    price: 15.00,
    category: 3
  }
];

for (let i = 0; i < items.length; i++) {
  console.log(`
    Name: ${items[i].name}
    Description: ${items[i].description}
    Price: ${items[i].price}
  `);
}

// Answer
 /*
    NO problem in the code. But id might be different
 */

// Question: What would be the output?

for (let row = 1; row < 3; row++) {
  for (let col = 1; col <= row; col++) {
    console.log(`Current row: ${row}, Current col: ${col}`);
  }
}
// Answer
/*
  Current row: 1, Current col: 1
  Current row: 2, Current col: 1
  Current row: 2, Current col: 2
*/

// Question: What would be the problem in the code snippet?

function checkLeapYear(year) {
  if (year % 4 = 0) {
    if (year % 100 = 0) {
      if (year % 400 = 0) {
        console.log('Leap year');
      } else {
        console.log('Not a leap year');
      }
    } else {
      console.log('Leap year');
    }
  } else {
    console.log('Not a leap year');
  }
}

checkLeapYear(1999);

// Answer
/*
  in the conditional statements instead of using conditional operators(===),the assignment operator was used(=)
*/

// Question: Given the array below, how can the last student's English grade be displayed?

let records = [
  {
    id: 1,
    name: 'Brandon',
    subjects: [
      { name: 'English', grade: 98 },
      { name: 'Math', grade: 66 },
      { name: 'Science', grade: 87 }
    ]
  },
  {
    id: 2,
    name: 'Jobert',
    subjects: [
      { name: 'English', grade: 87 },
      { name: 'Math', grade: 99 },
      { name: 'Science', grade: 74 }
    ]
  },
  {
    id: 3,
    name: 'Junson',
    subjects: [
      { name: 'English', grade: 60 },
      { name: 'Math', grade: 99 },
      { name: 'Science', grade: 87 }
    ]
  }
];

// Answer
/*
  records[2].subjects[0].grade
*/

// Question: What would be the problem in the code snippet?

function checkDivisibility(dividend, divisor) {
  if (dividend % divisor == 0) {
    console.log(`${dividend} is divisible by ${divisor}`);
  } else {
    console.log(`${dividend} is not divisible by ${divisor}`);
  }
}

checkDivisibility(100, 0);

// Answer

/*
  Divisibility with zero should be undefined but here it will show not divisible. so we have to add a base case that if divisor ==0 then it should return undefined.  
*/