Session s50:
	create a s50-s55 folder
		inside the folder run, npx create-react-app react-app
			Wait for the "Happy hacking" sign
		cd react-app
			touch guide.js
			npm start, to start the application
			remove the unnecessary files:
				App.test.js
				index.css
				logo.svg
				reportWebVitals.js
			install bootstrap dependencies
				npm install react-bootstrap bootstrap
			create a components folder in src 
				create AppNavbar.js
				create Banner.js
				create Highlights.js
				refactor App.js
			create a pages folder in src
				create Home.js
		Activity
			create CourseCard.js
			create a Courses.js in pages
				create a state Hook
				