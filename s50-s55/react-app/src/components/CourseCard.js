
/*
Activity
export default function CourseCard() {
    return(
        <Card className="p-3">
            <Card.Title>
                Sample Course
            </Card.Title>
            <Card.Body className="Card-Body  p-0">
                <Card.Text>
                    <b>Description:</b>
                    <p>This is a sample course offering</p>
                    <b>Price:</b>
                    <p>Php 40,000</p>
                </Card.Text>
                <Button>Enroll</Button>
            </Card.Body>
        </Card>
    ) 
}
*/
/*

export default function CourseCard() {
    return (
        <Card>
            <Card.Body>
                <Card.Title>Sample Course</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>This is a sample course offering!</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>Php 40,000</Card.Text>
                <Button variant="primary">Enroll</Button>
            </Card.Body>
        </Card>
    );
};
*/

import { Card, Button,Col,Row } from 'react-bootstrap';
import {useState,useEffect} from 'react';
import {Link} from 'react-router-dom';

export default function CourseCard({course}) {
    const {name,description,price,_id} = course;
    // React Hook that lets you add a state variable to the components.
    // Use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components.
    // Syntax:
        // const [getter,setter] = useState(initialGetterValue)
// State Hook



// const [count,setCount] = useState(0);
// const [seats,setSeats] = useState(30);
// const [isOpen,setIsOpen] = useState(true);

// function enroll(){
//     if(seats>0){
//         setCount(count+1);
//         console.log('Enrollees: '+count);
//         setSeats(seats-1);
//         console.log('Seats: '+ seats);
//      } //else {
//     //     alert("No more seats available!");
//     // };
// };
    // useEffect - allows us to instruct the app that the component needs to do something
    // Keyword for useEffect, "reactive". It makes our app, reactive.

    // useEffect(()=>{
    //     if(count ===30 && seats === 0){
    //         setIsOpen(false);
    //         alert("No more Seats!");
    //         document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled',true);  
    //     }
    // },[ count,seats ])

    return (
        
        <Row className = "mt-3 mb-3">
            <Col xs={12}>
                <Card className = "cardHighlight p-0">
                     <Card.Body>
                        <Card.Title><h4>{name}</h4></Card.Title>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>{price}</Card.Text>
                        
                        <Button className="bg-primary" as ={Link} to ={`/courses/${_id}`}>Enroll</Button>
                    </Card.Body>
                </Card>
            </Col>
        </Row>  
    );
};


/*
// Activity s51
const [count,setCount] = useState(0);
const [seats,countSeats] = useState(30);
function enroll(){
    if(seats===0){
        alert('no more Seats');
    } else{
        setCount(count+1);
        countSeats(seats-1);
    }; 
};

    return (
        
        <Row className = "mt-3 mb-3">
            <Col xs={12}>
                <Card className = "cardHighlight p-0">
                     <Card.Body>
                        <Card.Title><h4>{name}</h4></Card.Title>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>{price}</Card.Text>
                        <Card.Subtitle>Enrollees:{count}</Card.Subtitle>
                        
                        <Button variant="primary" onClick={enroll}>Enroll</Button>
                    </Card.Body>
                </Card>
            </Col>
        </Row>  
    );
};
*/
